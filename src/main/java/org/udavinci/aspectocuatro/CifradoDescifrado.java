package org.udavinci.aspectocuatro;

import java.util.Scanner;

public class CifradoDescifrado {

    private static Scanner entrada;
    private static int numero;
    private static int numeroCifrado;
    private static int temp;
    private static int cont;
    private static int[] digitos;
    private static int digito;

    public static void main(String[] args) {

        entrada = new Scanner(System.in);
        do {
            System.out.printf("\nIntroduzca un número de 4 dígitos: ");
            while (!entrada.hasNextInt()) {
                System.out.printf("\"%s\" es un formato incorrecto.\nIntroduzca un número de 4 dígitos: ",
                        entrada.next());
            }
            numero = entrada.nextInt();
        }while(numero < 1000 || numero >= 10000);

        cifrarDescifrar(numero, 7);

        numeroCifrado = Integer.parseInt(valorCifradoString(digitos));
        System.out.printf("\nEl número cifrado es: %d%d%d%d", digitos[0], digitos[1], digitos[2], digitos[3]);

        cifrarDescifrar(numeroCifrado, 3);

        System.out.printf("\nEl número descifrado es: %d%d%d%d", digitos[0], digitos[1], digitos[2], digitos[3]);
    }

    private static void cifrarDescifrar(int numero, int valor){
        temp = 1000;
        cont = 0;
        digitos = new int[4];

        while (numero != 0){
            digito = numero/temp;
            cont ++;

            switch (cont){
                case 1:
                    digitos[0] = (valor + digito)%10;
                    break;
                case 2:
                    digitos[1] = (valor + digito)%10;
                    break;
                case 3:
                    digitos[2] = (valor + digito)%10;
                    break;
                case 4:
                    digitos[3] = (valor + digito)%10;
                    break;
            }

            numero %= temp;
            if (temp != 0){
                temp/=10;
            }
        }
    }

    private static String valorCifradoString(int[] cifras){
        return String.valueOf(cifras[0]) + String.valueOf(cifras[1]) + String.valueOf(cifras[2]) + String.valueOf(cifras[3]);
    }
}
