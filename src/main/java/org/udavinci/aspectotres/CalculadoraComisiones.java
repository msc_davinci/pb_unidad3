package org.udavinci.aspectotres;

import java.util.Scanner;

public class CalculadoraComisiones {
    private static double[] articulos;
    private static double[] articulosVendidos;
    private static Scanner entrada;
    private static int vendedores;
    private static int item;
    private static double sumaArticulos;
    private static double totalIngresos;
    private static double comisionSemanal= 200;
    private static double comisionVar= 0.09;

    public static void main(String[] args) {
        System.out.println("\n***************** Bienvenido al Sistema de Comisiones por Venta ***************************\n");
        ingresoNumeroVendedores();
        for (int i=1; i <= vendedores; i++){
            ingresoArtículosVendidos(i);
        }
        System.out.println("\n****************************************************************************************\n");
    }

    public static void ingresoNumeroVendedores(){
        entrada = new Scanner(System.in);
        vendedores = 0;
        while (vendedores <= 0){
            System.out.print("¿Cuántos vendedores quiere ingresar? (debe ser un número mayor a 0): ");
            while (!entrada.hasNextInt()) {
                System.out.printf("\"%s\" no es un número.\nFavor de ingresar el número de vendedores (debe ser un número mayor a 0): ",
                        entrada.next());
            }
            vendedores = entrada.nextInt();
        }
    }

    public static void ingresoArtículosVendidos(int i){
        entrada = new Scanner(System.in);
        articulosVendidos = new double[4];
        articulos = new double[4];
        sumaArticulos = 0;
        item = 0;
        comisionSemanal= 200; // weekly fixed commision
        comisionVar= 0.09;
        articulos[0]= 239.99;
        articulos[1]= 129.75;
        articulos[2]= 99.95;
        articulos[3]= 350.89;

        System.out.printf("\nIngresa los artículos del vendedor %d\n", i);

        for(int j=0; j<=3; j++){
            do {
                System.out.printf("Ingresa la cántidad de items vendidos del artículo %d:", j+1);
                while (!entrada.hasNextInt()) {
                    System.out.printf("\"%s\" es un formato incorrecto.\nIngresa la cántidad de items vendidos del artículo %d: (debe ser un número mayor a 0): ",
                            entrada.next(),j+1);
                }
                item = entrada.nextInt();
            }while(item < 1);

            articulosVendidos[j] = item;
            articulosVendidos[j] *= articulos[j];
        }

        for (double totalArticulos: articulosVendidos) {
            sumaArticulos += totalArticulos;
        }

        System.out.printf("\nLa cantidad total vendida es: $%.2f", sumaArticulos);
        totalIngresos= comisionSemanal + (sumaArticulos*comisionVar);
        System.out.printf ("\nLa comisión semanal del vendedor %d es: $%.2f\n", i, totalIngresos);
    }
}
