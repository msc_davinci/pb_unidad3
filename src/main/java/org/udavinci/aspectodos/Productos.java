package org.udavinci.aspectodos;

import java.util.Scanner;

public class Productos {
    private static int numero_producto;
    private static float costo;
    private static float monto_total = 0;
    private static int cantidad_vendida;
    private static Scanner entrada;
    private static double[] precios;

    public static void main(String[] args) {
        entrada = new Scanner(System.in);
        precios = new double[5];
        precios[0] = 2.98;
        precios[1] = 4.50;
        precios[2] = 9.98;
        precios[3] = 4.49;
        precios[4] = 6.87;

        System.out.print("Este almacen vende cinco productos,");
        System.out.println("a continuación los precios:");
        System.out.printf("\nProducto 1: $%.2f", precios[0]);
        System.out.printf("\nProducto 2: $%.2f", precios[1]);
        System.out.printf("\nProducto 3: $%.2f", precios[2]);
        System.out.printf("\nProducto 4: $%.2f", precios[3]);
        System.out.printf("\nProducto 5: $%.2f\n", precios[4]);

        System.out.println("\nPor favor introduzca el numero del primer producto:");
        System.out.println("NOTA: escriba -1 para terminar.");
        numero_producto = entrada.nextInt();

        while (-1 != numero_producto) {
            switch (numero_producto) {
                case 1: {
                    monto_total += cantidadPrecioProducto(precios[0]) * precios[0];
                    break;
                }
                case 2: {
                    monto_total += cantidadPrecioProducto(precios[1]) * precios[1];
                    break;
                }
                case 3: {
                    monto_total += cantidadPrecioProducto(precios[2]) * precios[2];
                    break;
                }
                case 4: {
                    monto_total += cantidadPrecioProducto(precios[3]) * precios[3];
                    break;
                }
                case 5: {
                    monto_total += cantidadPrecioProducto(precios[4]) * precios[4];
                    break;
                }

                default: {
                    System.out.print("\nINTRODUZCA NUMERO DE PRODUCTO VALIDO!");
                    break;
                }
            }
            System.out.println("\nPor favor introduzca el numero del siguiente producto:");
            System.out.println("NOTA: escriba -1 para terminar.");
            numero_producto = entrada.nextInt();

        }
        System.out.printf("\nLas ventas totales fueron: $%.2f\n\n", monto_total);
    }

    public static int cantidadPrecioProducto(double precio){
        System.out.printf("Introduzca la cantidad vendida de este producto:");
        cantidad_vendida = entrada.nextInt();
        System.out.printf("Precio de esta venta: $%.2f", cantidad_vendida * precio);
        return cantidad_vendida;
    }
}